// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include <GL/glew.h>

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#if !wxUSE_GLCANVAS
#error "OpenGL required: set wxUSE_GLCANVAS to 1 and rebuild the library"
#endif

#include "mxw_glcanvas.h"
#include "MXwendler_01.h"

DECLARE_APP(mxwApp)

mxwGLCanvas::mxwGLCanvas(wxWindow *parent)
    // With perspective OpenGL graphics, the wxFULL_REPAINT_ON_RESIZE style
    // flag should always be set, because even making the canvas smaller should
    // be followed by a paint event that updates the entire canvas with new
    // viewport settings.
    : wxGLCanvas(parent, wxID_ANY, NULL,
                 wxDefaultPosition, wxDefaultSize,
                 wxFULL_REPAINT_ON_RESIZE)
{
    m_glContext2 = new wxGLContext(this);
    Bind(wxEVT_PAINT, &mxwGLCanvas::OnPaint, this);
    m_timer.Bind(wxEVT_TIMER, &mxwGLCanvas::OnTimer, this);
    m_timer.Start(25);
}


void mxwGLCanvas::OnPaint(wxPaintEvent &WXUNUSED(event))
{
    // This is required even though dc is not used otherwise.
    wxPaintDC dc(this);

    // Set the OpenGL viewport according to the client size of this canvas.
    // This is done here rather than in a wxSizeEvent handler because our
    // OpenGL rendering context (and thus viewport setting) is used with
    // multiple canvases: If we updated the viewport in the wxSizeEvent
    // handler, changing the size of one canvas causes a viewport setting that
    // is wrong when next another canvas is repainted.
    const wxSize ClientSize = GetClientSize();

    //m_glContext2 = wxGetApp().GetContext(this);

    static bool init_glcontext = false;

    if ( !init_glcontext )
    {
        // Create the OpenGL context for the first mono window which needs it:
        // subsequently created windows will all share the same context.
        
        
        SetCurrent(*m_glContext2);//is same as: m_glContext->SetCurrent(*canvas);

        InitOpenGL();//glew and imgui init stuff

        InitGL();// point cube init stuff

        init_glcontext = true;
    }






    m_glContext2->SetCurrent(*this);


    ImGuiIO& io = ImGui::GetIO(); 
    
    //io.DisplaySize = ImVec2(640.0, 480.0);
    ImGui_ImplMXW_NewFrame_temp();
    ImGui_ImplWxWidgets_ProcessEvent(this);
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplWxWidgets_NewFrame();
    ImGui::NewFrame();
	static bool show_demo_window = true; 
    if (show_demo_window)  ImGui::ShowDemoWindow(&show_demo_window);


    ImGui::Begin("new win");
    {
        ImVec2 winpos = ImGui::GetWindowPos();
        ImVec2 mousepos = ImGui::GetMousePos();
        //ImVec2 mousepos = ImGui::GetMousePos();

        ImGui::Text("winpos = %.3f , %.3f ",winpos.x,winpos.y );
        ImGui::Text("mousepos = %.3f , %.3f ",mousepos.x,mousepos.y );
        ImGui::SetNextWindowPos(ImVec2(800.0, 400.0));
        if(ImGui::Button("create"))
        {
            //mxwFrame * dd = new mxwFrame();
            CreateNewWindow();
        }
    }
    ImGui::End();

    ImGui::Render();





    glViewport(0, 0, ClientSize.x, ClientSize.y);

    m_xangle += 0.1;
    m_yangle += 0.1;
    DrawRotatedCube(m_xangle, m_yangle);




    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());


    // Update and Render additional Platform Windows
    // (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
    //  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        //GLFWwindow* backup_current_context = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        //glfwMakeContextCurrent(backup_current_context);
    }

    // if(testttt)
    // {
    //     //testttt->DrawTriangle();
    // }

    SwapBuffers();
}

void mxwGLCanvas::CreateNewWindow()
{
    if(1)
    {
        if(0)
        {
            // wxFrame * fff = new wxFrame(NULL, wxID_ANY, wxT("wxWidgets OpenGL Cube Sample"));
            // mxwGLCanvas * can = new mxwGLCanvas(fff, m_glContext2);  // content created inside?
            // fff->SetClientSize(400, 400);
            // fff->Show();
        }
        else
        {

                wxFrame * fff = new wxFrame(NULL, wxID_ANY, wxT("wxWidgets OpenGL Cube Sample"));
                
                wxGLCanvas *testttt = new wxGLCanvas(fff, wxID_ANY, NULL,
                 wxDefaultPosition, wxDefaultSize,
                 wxFULL_REPAINT_ON_RESIZE);

                wxGLContext *m_glContext2dd = new wxGLContext(testttt);
                //can->SetCurrent(*m_glContext2dd);
                fff->SetClientSize(400, 400);
                fff->Show();


            // wxFrame * fff = new wxFrame(NULL, wxID_ANY, wxT("wxWidgets OpenGL Cube Sample"));
            // wxGLCanvas * can = new wxGLCanvas(fff, wxID_ANY, NULL,
            //      wxDefaultPosition, wxDefaultSize,
            //      wxFULL_REPAINT_ON_RESIZE);

            // can->GetParent()->Move(10,10);     
            // can->GetParent()->SetSize(500,700);   

            // wxString ddd = "test";
            // can->GetParent()->SetLabel(ddd);

            // //can->Move(10,10);          
        }


    }
    else
    {
        wxFrame * fff = new wxFrame(NULL, wxID_ANY, wxT("wxWidgets OpenGL Cube Sample"));
        wxGLCanvas * can = new wxGLCanvas(fff, wxID_ANY, NULL,
        wxDefaultPosition, wxDefaultSize,
        wxFULL_REPAINT_ON_RESIZE);
        fff->SetClientSize(400, 400);
        fff->Show();        
        can->SetCurrent(*m_glContext2);//same as : m_glContext2->SetCurrent(*can);
        // this makes no sence since it needs to be set on paint
    }
}

void mxwGLCanvas::OnTimer(wxTimerEvent &WXUNUSED(event))
{
    Refresh(false);
}

void mxwGLCanvas::InitOpenGL()
{
    
    //
    // define declare opengl rendering
    if(glewInit() !=GLEW_OK )
        glClearColor(0.9f, 0.0f, 0.0f,1.0f);
    else
        glClearColor(0.3f, 0.4f, 0.1f,1.0f);

    // some foo stuff to test if modern opengl is there
    {
        float possitions[6] = {-0.5f,-0.5f,0.0f,0.5f,0.5f,-0.5f };
        unsigned int buffer;
        glGenBuffers(1,&buffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, 6*sizeof(float),possitions,GL_STATIC_DRAW);    
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLAT, GL_FALSE, 2*sizeof(float),0);
    }

    //
    // init imgui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
    io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows

    ImGuiStyle& style = ImGui::GetStyle();
    ImGui_ImplWxWidgets_InitForOpenGL();
    ImGui_ImplOpenGL3_Init("#version 130");
   
}

bool mxwGLCanvas::ImGui_ImplMXW_NewFrame_temp()
{
    static double       g_Time = 0.0f;
    static double       millis_double = 0;

    millis_double += 0.01;
    ImGuiIO& io = ImGui::GetIO();

    wxSize ddd = GetSize();
	io.DisplaySize = ImVec2((float)ddd.x, (float)ddd.y);
    io.DisplayFramebufferScale = ImVec2(1, 1);

    double current_time = millis_double;
	io.DeltaTime = g_Time > 0.0 ? (float)(current_time - g_Time) : (float)(1.0f / 60.0f);

    g_Time = current_time;





    return true;
}


///////////////////////////////////////    pointing cube


// function to draw the texture for cube faces
wxImage mxwGLCanvas::DrawDice(int size, unsigned num)
{
    wxASSERT_MSG( num >= 1 && num <= 6, wxT("invalid dice index") );

    const int dot = size/16;        // radius of a single dot
    const int gap = 5*size/32;      // gap between dots

    wxBitmap bmp(size, size);
    wxMemoryDC dc;
    dc.SelectObject(bmp);
    dc.SetBackground(*wxWHITE_BRUSH);
    dc.Clear();
    dc.SetBrush(*wxBLACK_BRUSH);

    // the upper left and lower right points
    if ( num != 1 )
    {
        dc.DrawCircle(gap + dot, gap + dot, dot);
        dc.DrawCircle(size - gap - dot, size - gap - dot, dot);
    }

    // draw the central point for odd dices
    if ( num % 2 )
    {
        dc.DrawCircle(size/2, size/2, dot);
    }

    // the upper right and lower left points
    if ( num > 3 )
    {
        dc.DrawCircle(size - gap - dot, gap + dot, dot);
        dc.DrawCircle(gap + dot, size - gap - dot, dot);
    }

    // finally those 2 are only for the last dice
    if ( num == 6 )
    {
        dc.DrawCircle(gap + dot, size/2, dot);
        dc.DrawCircle(size - gap - dot, size/2, dot);
    }

    dc.SelectObject(wxNullBitmap);

    return bmp.ConvertToImage();
}

void mxwGLCanvas::InitGL()
{
    // set up the parameters we want to use
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_TEXTURE_2D);

    // add slightly more light, the default lighting is rather dark
    GLfloat ambient[] = { 0.5, 0.5, 0.5, 0.5 };
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

    // set viewing projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.5f, 0.5f, -0.5f, 0.5f, 1.0f, 3.0f);

    // create the textures to use for cube sides: they will be reused by all
    // canvases (which is probably not critical in the case of simple textures
    // we use here but could be really important for a real application where
    // each texture could take many megabytes)
    glGenTextures(WXSIZEOF(m_textures), m_textures);

    for ( unsigned i = 0; i < WXSIZEOF(m_textures); i++ )
    {
        glBindTexture(GL_TEXTURE_2D, m_textures[i]);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        const wxImage img(DrawDice(256, i + 1));

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.GetWidth(), img.GetHeight(),
                     0, GL_RGB, GL_UNSIGNED_BYTE, img.GetData());
    }
}



void mxwGLCanvas::DrawRotatedCube(float xangle, float yangle)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -2.0f);
    glRotatef(xangle, 1.0f, 0.0f, 0.0f);
    glRotatef(yangle, 0.0f, 1.0f, 0.0f);

    // draw six faces of a cube of size 1 centered at (0, 0, 0)
    glBindTexture(GL_TEXTURE_2D, m_textures[0]);
    glBegin(GL_QUADS);
        glNormal3f( 0.0f, 0.0f, 1.0f);
        glTexCoord2f(0, 0); glVertex3f( 0.5f, 0.5f, 0.5f);
        glTexCoord2f(1, 0); glVertex3f(-0.5f, 0.5f, 0.5f);
        glTexCoord2f(1, 1); glVertex3f(-0.5f,-0.5f, 0.5f);
        glTexCoord2f(0, 1); glVertex3f( 0.5f,-0.5f, 0.5f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, m_textures[1]);
    glBegin(GL_QUADS);
        glNormal3f( 0.0f, 0.0f,-1.0f);
        glTexCoord2f(0, 0); glVertex3f(-0.5f,-0.5f,-0.5f);
        glTexCoord2f(1, 0); glVertex3f(-0.5f, 0.5f,-0.5f);
        glTexCoord2f(1, 1); glVertex3f( 0.5f, 0.5f,-0.5f);
        glTexCoord2f(0, 1); glVertex3f( 0.5f,-0.5f,-0.5f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, m_textures[2]);
    glBegin(GL_QUADS);
        glNormal3f( 0.0f, 1.0f, 0.0f);
        glTexCoord2f(0, 0); glVertex3f( 0.5f, 0.5f, 0.5f);
        glTexCoord2f(1, 0); glVertex3f( 0.5f, 0.5f,-0.5f);
        glTexCoord2f(1, 1); glVertex3f(-0.5f, 0.5f,-0.5f);
        glTexCoord2f(0, 1); glVertex3f(-0.5f, 0.5f, 0.5f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, m_textures[3]);
    glBegin(GL_QUADS);
        glNormal3f( 0.0f,-1.0f, 0.0f);
        glTexCoord2f(0, 0); glVertex3f(-0.5f,-0.5f,-0.5f);
        glTexCoord2f(1, 0); glVertex3f( 0.5f,-0.5f,-0.5f);
        glTexCoord2f(1, 1); glVertex3f( 0.5f,-0.5f, 0.5f);
        glTexCoord2f(0, 1); glVertex3f(-0.5f,-0.5f, 0.5f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, m_textures[4]);
    glBegin(GL_QUADS);
        glNormal3f( 1.0f, 0.0f, 0.0f);
        glTexCoord2f(0, 0); glVertex3f( 0.5f, 0.5f, 0.5f);
        glTexCoord2f(1, 0); glVertex3f( 0.5f,-0.5f, 0.5f);
        glTexCoord2f(1, 1); glVertex3f( 0.5f,-0.5f,-0.5f);
        glTexCoord2f(0, 1); glVertex3f( 0.5f, 0.5f,-0.5f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, m_textures[5]);
    glBegin(GL_QUADS);
        glNormal3f(-1.0f, 0.0f, 0.0f);
        glTexCoord2f(0, 0); glVertex3f(-0.5f,-0.5f,-0.5f);
        glTexCoord2f(1, 0); glVertex3f(-0.5f,-0.5f, 0.5f);
        glTexCoord2f(1, 1); glVertex3f(-0.5f, 0.5f, 0.5f);
        glTexCoord2f(0, 1); glVertex3f(-0.5f, 0.5f,-0.5f);
    glEnd();

    glFlush();

    //CheckGLError();
}
