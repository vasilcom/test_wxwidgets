
#pragma once



#include "mxw_glcanvas.h"

class mxwApp;
// Define a new frame type




class mxwFrame : public wxFrame
{
public:
    mxwFrame();
    ~mxwFrame();

    mxwGLCanvas * getCanvas() const {return main_canvas;}
    mxwGLCanvas * main_canvas = nullptr;

private:
    void OnClose(wxCommandEvent& event);
    void OnNewWindow(wxCommandEvent& event);

    

    wxDECLARE_EVENT_TABLE();
};

