
#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "mxw_frame.h"
//#include "mxw_context.h"
// Define a new application type



// Define a new application type
class mxwApp : public wxApp
{
public:
    mxwApp() {  }

    // Returns the shared context used by all frames and sets it as current for
    // the given canvas.
   
    // virtual wxApp methods
    virtual bool OnInit();
    virtual int OnExit();
    mxwFrame* getFrame() const {return m_frame;}
    
private:
    // the GL context we use for all our mono rendering windows

    mxwFrame * m_frame;
};