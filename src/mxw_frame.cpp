//--------------------------------------------

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#if !wxUSE_GLCANVAS
    #error "OpenGL required: set wxUSE_GLCANVAS to 1 and rebuild the library"
#endif

#include "mxw_frame.h"
#include "MXwendler_01.h"


wxBEGIN_EVENT_TABLE(mxwFrame, wxFrame)
    EVT_MENU(wxID_NEW, mxwFrame::OnNewWindow)
    EVT_MENU(wxID_CLOSE, mxwFrame::OnClose)
wxEND_EVENT_TABLE()

mxwFrame::mxwFrame()
       : wxFrame(NULL, wxID_ANY, wxT("wxWidgets OpenGL Cube Sample"))
{
    

    main_canvas = new mxwGLCanvas(this);

   

    // Make a menubar
    wxMenu *menu = new wxMenu;
    menu->Append(wxID_NEW);
    menu->AppendSeparator();
    menu->Append(wxID_CLOSE);
    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append(menu, wxT("&Cube"));

    SetMenuBar(menuBar);

    CreateStatusBar();

    SetClientSize(800, 600);
    Show();

}

mxwFrame::~mxwFrame()
{
    delete main_canvas;
}

void mxwFrame::OnClose(wxCommandEvent& WXUNUSED(event))
{
    // true is to force the frame to close
    Close(true);
}

void mxwFrame::OnNewWindow( wxCommandEvent& WXUNUSED(event) )
{
    main_canvas->CreateNewWindow();

}
