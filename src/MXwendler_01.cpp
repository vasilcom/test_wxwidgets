
#include "MXwendler_01.h"


IMPLEMENT_APP(mxwApp)
DECLARE_APP(mxwApp)

bool mxwApp::OnInit()
{
    if ( !wxApp::OnInit() )
        return false;

    m_frame = new mxwFrame();

    return true;
}

int mxwApp::OnExit()
{

    return wxApp::OnExit();
}
