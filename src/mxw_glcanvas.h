#pragma once
#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_wxwidgets.h"
#include <GL/glew.h>

#include "wx/glcanvas.h"
class mxwApp;
class wxGLContext;

class mxwGLCanvas : public wxGLCanvas
{
public:
    mxwGLCanvas(wxWindow *parent);
    void CreateNewWindow();
    bool ImGui_ImplMXW_NewFrame_temp();
    wxGLContext *m_glContext2;// vm_todo: make it shared pointer to avoit memory leak
    wxGLContext* getContext() const {return m_glContext2;}
private:

    void InitOpenGL();
    void OnPaint(wxPaintEvent& event);   

    // angles of rotation around x- and y- axis
    float m_xangle = 30.0;
    float m_yangle = 30.0;

    wxTimer m_timer;
    void OnTimer(wxTimerEvent&);



    // render the cube showing it at given angles
    wxImage DrawDice(int size, unsigned num);
    void DrawRotatedCube(float xangle, float yangle);
    void InitGL();
    // textures for the cube faces
    GLuint m_textures[6];


};
