#pragma once
#include "imgui.h"
class wxGLCanvas;

class wxFrame;
class wxGLContext;


IMGUI_IMPL_API void ImGui_ImplWxWidgets_InitForOpenGL(); // BackendFlags, ImGui_ImplWxWidgets_UpdateMonitors, ImGui_ImplWxWidgets_InitPlatformInterface
IMGUI_IMPL_API void ImGui_ImplWxWidgets_NewFrame(); // foo
IMGUI_IMPL_API void ImGui_ImplWxWidgets_Shutdown(); // foo
IMGUI_IMPL_API void ImGui_ImplWxWidgets_ProcessEvent(wxGLCanvas * can); // mouse events