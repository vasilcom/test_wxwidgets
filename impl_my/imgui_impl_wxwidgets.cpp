#include <GL/glew.h>
#include "imgui_impl_wxwidgets.h"

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include "wx/glcanvas.h" 
#include "mxw_glcanvas.h"
#include <wx/display.h>

#include <GL/glew.h>

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <iostream>
#include <cstdint>
#include <cstring>
 #include "MXwendler_01.h"
DECLARE_APP(mxwApp)

struct ImGui_ImplWxWidgets_Data
{
    wxGLCanvas*     Window = nullptr;

    ImGui_ImplWxWidgets_Data()   { memset((void*)this, 0, sizeof(*this)); }
};

struct ImGui_ImplWxWidgets_ViewportData
{
    wxGLCanvas*        Window = nullptr;

    ImGui_ImplWxWidgets_ViewportData() { Window = NULL; }
    ~ImGui_ImplWxWidgets_ViewportData() { { IM_ASSERT(Window == NULL ); } }
};

static ImGui_ImplWxWidgets_Data* ImGui_ImplWxWidgets_GetBackendData()
{	
	return ImGui::GetCurrentContext() ? (ImGui_ImplWxWidgets_Data*)ImGui::GetIO().BackendPlatformUserData : NULL;

}


//
// platform_io pointer functions 

static void ImGui_ImplWxWidgets_CreateWindow(ImGuiViewport* viewport)
{
    ImGui_ImplWxWidgets_Data* bd = ImGui_ImplWxWidgets_GetBackendData();
    ImGui_ImplWxWidgets_ViewportData* vd = IM_NEW(ImGui_ImplWxWidgets_ViewportData)();
    viewport->PlatformUserData = vd;

	ImGuiViewport* main_viewport = ImGui::GetMainViewport();
	ImGui_ImplWxWidgets_ViewportData* main_viewport_data = (ImGui_ImplWxWidgets_ViewportData*)main_viewport->PlatformUserData;

	wxFrame * new_fr = new wxFrame(main_viewport_data->Window, wxID_ANY, wxT("No Title Yet"));	
	new_fr->Show();
	vd->Window = new wxGLCanvas(new_fr, wxID_ANY, NULL,
                 wxDefaultPosition, wxDefaultSize,
                 wxFULL_REPAINT_ON_RESIZE);

	IM_ASSERT(vd->Window);

    viewport->PlatformHandle = vd->Window;
}

static void ImGui_ImplWxWidgets_DestroyWindow(ImGuiViewport* viewport)
{
	if (ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData)
    {
        vd->Window->GetParent()->Close();    
        vd->Window = NULL;
        IM_DELETE(vd);
    }
    viewport->PlatformUserData = viewport->PlatformHandle = NULL;
}

static void ImGui_ImplWxWidgets_ShowWindow(ImGuiViewport* viewport)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
	vd->Window->GetParent()->Show();
}

static void ImGui_ImplWxWidgets_SetWindowPos(ImGuiViewport* viewport, ImVec2 pos)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
	vd->Window->GetParent()->Move((int)pos.x,(int)pos.y);			
}

static ImVec2 ImGui_ImplWxWidgets_GetWindowPos(ImGuiViewport* viewport)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
	wxPoint rr = vd->Window->GetScreenPosition();
	return ImVec2((float)rr.x, (float)rr.y);
}

static void ImGui_ImplWxWidgets_SetWindowSize(ImGuiViewport* viewport, ImVec2 size)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
    vd->Window->GetParent()->SetSize((int)size.x,(int)size.y);  
}

static ImVec2 ImGui_ImplWxWidgets_GetWindowSize(ImGuiViewport* viewport)
{	
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
	wxSize rr = vd->Window->GetSize();
	return ImVec2((float)rr.x, (float)rr.y);
}

static void ImGui_ImplWxWidgets_SetWindowTitle(ImGuiViewport* viewport, const char* title)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
	wxString wx_title = wxString::FromUTF8(title);
	vd->Window->GetParent()->SetLabel(wx_title);
}

static void ImGui_ImplWxWidgets_RenderWindow(ImGuiViewport* viewport, void*)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;
	IM_ASSERT(vd->Window);
	wxGLContext * c = wxGetApp().getFrame()->getCanvas()->getContext();
	vd->Window->SetCurrent(*c); // without this, the render content remaines on the main canvas
}

static void ImGui_ImplWxWidgets_SwapBuffers(ImGuiViewport* viewport, void*)
{
	ImGui_ImplWxWidgets_ViewportData* vd = (ImGui_ImplWxWidgets_ViewportData*)viewport->PlatformUserData;	
	IM_ASSERT(vd->Window);
	vd->Window->SwapBuffers();
}



//
// initialization

static void ImGui_ImplWxWidgets_UpdateMonitors()
{
	ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
	const size_t monitors_count = wxDisplay::GetCount();
	platform_io.Monitors.resize(0);
	for (int n = 0; n < monitors_count; n++)
    {
        ImGuiPlatformMonitor monitor;
		wxRect r = wxDisplay(n).GetGeometry();

        monitor.MainPos = monitor.WorkPos = ImVec2((float)r.x, (float)r.y);
        monitor.MainSize = monitor.WorkSize = ImVec2((float)r.width, (float)r.height);

        platform_io.Monitors.push_back(monitor);
    }
}

static void ImGui_ImplWxWidgets_InitPlatformInterface()
{
	ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
    platform_io.Platform_CreateWindow = ImGui_ImplWxWidgets_CreateWindow;
    platform_io.Platform_DestroyWindow = ImGui_ImplWxWidgets_DestroyWindow;
    platform_io.Platform_ShowWindow = ImGui_ImplWxWidgets_ShowWindow;
    platform_io.Platform_SetWindowPos = ImGui_ImplWxWidgets_SetWindowPos;
    platform_io.Platform_GetWindowPos = ImGui_ImplWxWidgets_GetWindowPos;
    platform_io.Platform_SetWindowSize = ImGui_ImplWxWidgets_SetWindowSize;
    platform_io.Platform_GetWindowSize = ImGui_ImplWxWidgets_GetWindowSize;
    platform_io.Platform_SetWindowTitle = ImGui_ImplWxWidgets_SetWindowTitle;
    platform_io.Platform_RenderWindow = ImGui_ImplWxWidgets_RenderWindow;
    platform_io.Platform_SwapBuffers = ImGui_ImplWxWidgets_SwapBuffers;
	
	ImGuiViewport* main_viewport = ImGui::GetMainViewport();
	ImGui_ImplWxWidgets_ViewportData* vd = IM_NEW(ImGui_ImplWxWidgets_ViewportData)();	
	
    vd->Window = wxGetApp().getFrame()->getCanvas();
    main_viewport->PlatformUserData = vd;
    main_viewport->PlatformHandle = vd->Window;
	IM_ASSERT(vd->Window);
}

void ImGui_ImplWxWidgets_InitForOpenGL()
{
    ImGuiIO& io = ImGui::GetIO();
    IM_ASSERT(io.BackendPlatformUserData == NULL && "Already initialized a platform backend!");

	// Setup backend capabilities flags
    ImGui_ImplWxWidgets_Data* bd = IM_NEW(ImGui_ImplWxWidgets_Data)();
	io.BackendPlatformUserData = (void*)bd;
	io.BackendPlatformName = "imgui_impl_wxwidgets";
	io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;         // We can honor GetMouseCursor() values (optional)
    io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;          // We can honor io.WantSetMousePos requests (optional, rarely used)
    io.BackendFlags |= ImGuiBackendFlags_PlatformHasViewports;    // We can create multi-viewports on the Platform side (optional)

    bd->Window = wxGetApp().getFrame()->getCanvas();
    IM_ASSERT(bd->Window);
	ImGui_ImplWxWidgets_UpdateMonitors();

	ImGui_ImplWxWidgets_InitPlatformInterface();
}

//
// frame update

void ImGui_ImplWxWidgets_NewFrame()
{

}

void ImGui_ImplWxWidgets_ProcessEvent(wxGLCanvas * canvas)
{
    static bool         g_MousePressed[3] = { false, false, false };
    
    ImGuiIO& io = ImGui::GetIO();
    wxMouseState ms = wxGetMouseState();
	for (int i = 0; i < 3; i++)
	{
		bool pressed = false;
		switch (i)
		{
			case 0:
				pressed = ms.LeftIsDown();
				break;
			case 1:
				pressed = ms.RightIsDown();
				break;
			case 2:
				pressed = ms.MiddleIsDown();
				break;
		}

		io.MouseDown[i] = g_MousePressed[i] || pressed;    // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
		g_MousePressed[i] = false;
	}

	const wxPoint pt = wxGetMousePosition();
 

	
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
		int mouseX = pt.x ;//- canvas->GetScreenPosition().x;
    	int mouseY = pt.y ;//- canvas->GetScreenPosition().y;
		io.AddMousePosEvent(mouseX, mouseY);
        // int window_x, window_y;
        // SDL_GetWindowPosition(SDL_GetWindowFromID(event->motion.windowID), &window_x, &window_y);
        // mouse_pos.x += window_x;
        // mouse_pos.y += window_y;
    }
	else
	{
		int mouseX = pt.x - canvas->GetScreenPosition().x;
    	int mouseY = pt.y - canvas->GetScreenPosition().y;
		io.AddMousePosEvent(mouseX, mouseY);
	}
	
}

// shutdown

void ImGui_ImplWxWidgets_Shutdown()
{

}