cmake_minimum_required(VERSION 3.4.1)

project(sandbox)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -fexceptions")

# set variables
set(MAIN_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(LIBS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/libs)
set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(IMPL_DIR ${CMAKE_CURRENT_SOURCE_DIR}/impl_my)
set(DESTINATION_DIR ${CMAKE_CURRENT_SOURCE_DIR}/out)

# set binary output dir
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${DESTINATION_DIR})


set( wxWidgets_CONFIG_OPTIONS ${wxWidgets_CONFIG_OPTIONS} "--toolkit=gtk3" ) #THIS IS ADDED FOR GTK

# check if wxWidgets is already installed in the system - using CMake's built in script FindwxWidgets
find_package(wxWidgets COMPONENTS core base qa adv net html gl propgrid richtext REQUIRED)
message(STATUS "Found preinstalled wxWidgets libraries at ${wxWidgets_LIBRARIES}")

add_library(wxWidgets_external INTERFACE)
find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)


# hack for buggy CMake's FindwxWidgets script
if (DEFINED ENV_WX_CONFIG)
    set (ENV{WX_CONFIG} ${ENV_WX_CONFIG})
endif()

set(wxWidgets_USE_STATIC 1)



find_package(wxWidgets COMPONENTS core base qa adv net html gl propgrid richtext REQUIRED)

# set source files 
file(GLOB_RECURSE SRC_FILES 
    ${SRC_DIR}/*.cpp
    ${SRC_DIR}/*.h
)
file(GLOB_RECURSE IMPL_FILES 
    ${IMPL_DIR}/*.cpp
    ${IMPL_DIR}/*.h
)
file(GLOB_RECURSE IMGUI_FILES 
    ${LIBS_DIR}/imgui/imgui_demo.cpp
    ${LIBS_DIR}/imgui/imgui_draw.cpp
    ${LIBS_DIR}/imgui/imgui_tables.cpp
    ${LIBS_DIR}/imgui/imgui_widgets.cpp
    ${LIBS_DIR}/imgui/imgui.cpp
    ${LIBS_DIR}/imgui/backends/imgui_impl_opengl3.cpp
)

include(${wxWidgets_USE_FILE})

# build executable
add_executable(${CMAKE_PROJECT_NAME}                            
                            ${SRC_FILES}
                            ${IMPL_FILES}
                            ${IMGUI_FILES})

target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC 
                            ${SRC_DIR}
                            ${IMPL_DIR}
                            ${LIBS_DIR}/imgui/backends
                            ${LIBS_DIR}/imgui
                            ${GLEW_INCLUDE_DIRS}
)

target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE GLEW::GLEW OpenGL::GL ${wxWidgets_LIBRARIES} ${CMAKE_DL_LIBS})