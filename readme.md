## build from command line on linux

    g++ `wx-config --cxxflags` -o myapp *.cpp -lGL -lGLU -lGLEW `wx-config --libs std,gl`

    or

    g++ `wx-config --cxxflags` -o myapp *.cpp -lGL -lGLEW `wx-config --libs std,gl`

    or

    g++ `wx-config --cxxflags` -o myapp *.cpp -lGL `wx-config --libs std,gl`

##  run:

    ./myapp

